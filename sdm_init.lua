local root = require "sdt" -- simple device tree

local drvlist = dofile("sdm_manifest.lua")

local function adddev(dev_table, parent)
   local dev = nil
   if parent == nil then
      dev = sdm.root()
   else
      dev = sdm.device_add(dev_table.name, parent)
   end
   if dev == nil then print(string.format("device %s not added", dev_table.name)); return end
   if dev_table.local_attributes ~= nil then
      for _, la in pairs(dev_table.local_attributes) do
         if sdm.local_attr_add(dev, la.name, la.desc, la.data, la.get, la.set) == nil then
            print(string.format("device %s local attribute %s not added",
                                sdm.device_name(dev),
                                la.name))
         end
      end
   end
   if not sdm.device_poll(dev) then print(string.format("not found driver for device %s", sdm.device_name(dev))) end
   if dev_table.children ~= nil then
      for _, ch in ipairs(dev_table.children) do
         adddev(ch, dev)
      end
   end
end

local function main()
   sdm.init() -- init library
   for _,d in ipairs(drvlist) do
      dofile(d) -- exec all driver scripts
   end
   adddev(root, nil) -- process device tree
end

main()
