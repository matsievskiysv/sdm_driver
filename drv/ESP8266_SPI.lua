local drv = sdm.driver_add("ESP8266_SPI")

sdm.method_add(drv, "_poll", nil, function(dev, drv, par) return (sdm.device_name(dev) == "ESP8266_SPI") and (sdm.device_name(par) == "ESP8266") end)

sdm.method_add(
   drv, "setup", "Setup SPI",
   function(bus, databits, clock_div)
      spi.setup(1, spi.MASTER, spi.CPOL_HIGH, spi.CPHA_HIGH, databits, clock_div, spi.FULLDUPLEX)
   end
)

sdm.method_add(
   drv, "exchange", "Exchange",
   function(bus, dev, data)
      local _, rv = spi.send(1, data)
      return rv
   end
)

drv = nil
