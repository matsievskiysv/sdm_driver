local drv = sdm.driver_add("DS18B20")

sdm.method_add(
   drv, "_poll", nil,
   function(dev, drv, par)
      local attr = sdm.attr_data(sdm.local_attr_handle(dev, "id"))
      if attr == nil then return false end
      return (sdm.device_name(par) == "ESP8266_1W") and (attr:byte(1) == 0x28)
   end
)

sdm.method_add(drv, "_init", nil,
               function(dev, drv, par)
                  sdm.device_rename(dev, sdm.request_name("DS18B20"))
                  sdm.attr_copy(dev, "temp")
                  sdm.attr_copy(dev, "precision")
                  local met = sdm.method_dev_handle(par, "setup")
                  local func = sdm.method_func(met)
                  func(par, dev)
               end
)

sdm.method_add(drv, "_free", nil,
               function(dev, drv, par)
                  local met = sdm.method_dev_handle(par, "free")
                  local func = sdm.method_func(met)
                  func(par, dev)
               end
)

sdm.method_add(drv, "measure", "Measure",
               function(dev)
                  local par = sdm.device_parent(dev)
                  local ex = sdm.method_func(sdm.method_dev_handle(par, "exchange"))
                  ex(par, dev, {0x44})
               end
)

sdm.attr_add(drv, "precision", "Precision (9|10|11|12)", 12,
               function(dev, precision)
                  local attr = sdm.attr_dev_handle(dev, "precision")
                  return sdm.attr_data(attr)
               end,
               function(dev, precision)
                  local par = sdm.device_parent(dev)
                  local attr = sdm.attr_dev_handle(dev, "precision")
                  local ex = sdm.method_func(sdm.method_dev_handle(par, "exchange"))
                  local modes = {[9]=0x1f, [10]=0x3f, [11]=0x5f, [12]=0x7f}
                  if modes[precision] ~= nil then
                     ex(par, dev, {0x4e, 0, 0, modes[precision]})
                     sdm.attr_set(attr, precision)
                  end
               end
)

sdm.attr_add(drv, "temp", "Temperature", "0",
             function(dev)
                local par = sdm.device_parent(dev)
                local attr = sdm.attr_dev_handle(dev, "temp")
                local pres = sdm.attr_data(sdm.attr_dev_handle(dev, "precision"))
                local ex = sdm.method_func(sdm.method_dev_handle(par, "exchange"))
                local modes = {[9]=0x8, [10]=0xc, [11]=0xe, [12]=0xf}
                local temp = ex(par, dev, {0xbe}, 2)
                local whole = bit.bor(bit.lshift(bit.band(temp[2], 0x7), 4), bit.rshift(temp[1], 4))
                local small = bit.band(temp[1], modes[pres])*625
                if bit.rshift(temp[2], 3) > 0 then
                   whole = whole * -1
                end
                if small < 1000 then
                   temp = string.format("%d.0%d", whole, small)
                else
                   temp = string.format("%d.%d", whole, small)
                end
                sdm.attr_set(attr, temp)
                return temp
             end,
             nil
)

drv = nil
